import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { hasFooter: true } // Include footer
    },
    {
      path: '/studio',
      name: 'studio',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/StudioView.vue'),
      meta: { hasFooter: true } // Include footer
    },
    {
      path: '/lab',
      name: 'lab',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LabView.vue'),
      meta: { hasFooter: true } // Include footer
    },
    {
      path: '/legal',
      name: 'Mentions légales',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LegalView.vue'),
      meta: { hasFooter: true } // Include footer
    },
    {
      path: '/contact',
      name: 'Contact',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/ContactView.vue'),
      meta: { hasFooter: true } // Include footer
    }
  ]
})

export default router
